
const int ledPin = 13;
void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);

}

/**
 * CÓDIGOS CORRESPONDIENTES A LOS DISTINTOS COMPONENTES:
 * 
 * -PRIMERA LETRA: SE CORRESPONDE CON EL COMPONENTE
 */
void loop() {

  if(Serial.available() > 0){

    String data= Serial.readString();
    Serial.println(data);
    char component = data.charAt(0);
    Serial.println(component);

    switch (component) {

      case 'L':
         led(data.substring(1));
         Serial.println(data.substring(1));
         break;

      default: 
        break;
    }
  }
}

void led(String accion){
  if(accion.equals("ON")){
    digitalWrite(ledPin, HIGH);
  }else if(accion.equals("OFF")){
    digitalWrite(ledPin, LOW);
  }else{
    Serial.println("MAL");
  }
  return;
}

