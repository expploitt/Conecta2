import socket
import serial

HOST = '192.168.0.13'
PORT = 5000
SERIAL_PORT = serial.Serial('/dev/ttyACM0', 9600)

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.bind((HOST, PORT))
sock.listen(1)

while True:

	socketCliente, addrCliente = sock.accept()
	print "Conectado a: ", addrCliente

	#Recibimos los datos procedentes del socket
	input = socketCliente.recv(4096)
	input = input[2:]
	#componente, input = data.split("*")
	print "%r" %(input)

	try:
		
		if (input == "LON") or (input == "LOFF"):
			SERIAL_PORT.write(input)
		else:
			print "Introduzca ON/OFF"
 	
	
	except EOFError:
		print "Saliendo..."
		time.sleep(1)
		exit(0)


