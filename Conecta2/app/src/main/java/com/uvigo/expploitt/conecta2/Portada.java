package com.uvigo.expploitt.conecta2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Portada extends AppCompatActivity {

    ImageView mImagenPortada;
    TextView mTextoPortada;
    EditText mDireccionIp, mPuertoIp;
    Button mBotonContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portada);

        mImagenPortada = (ImageView)findViewById(R.id.imagen_portada);
        mTextoPortada = (TextView)findViewById(R.id.cabecera_portada);
        mDireccionIp = (EditText)findViewById(R.id.ip_servidor);
        mPuertoIp = (EditText)findViewById(R.id.PORT);
        mBotonContinuar = (Button)findViewById(R.id.boton_continuar);






            mBotonContinuar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   if(!Utils.validacionDatos(Portada.this, mDireccionIp, mPuertoIp)){

                   }else {
                       GlobalParameters globalParameters = (GlobalParameters) getApplicationContext();
                       globalParameters.setIP(mDireccionIp.getText().toString());
                       globalParameters.setPORT(Integer.parseInt(mPuertoIp.getText().toString()));

                       Intent intent = new Intent(Portada.this, Opciones.class);
                       startActivity(intent);
                   }
                }
            });

    }
}
