package com.uvigo.expploitt.conecta2;

import android.content.Context;
import android.text.Editable;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by expploitt on 21/06/17.
 */

public class Utils {

    public static boolean validacionDatos(Context context, EditText edit1, EditText edit2){
        if(edit1.getText().toString().equals("") && edit2.getText().toString().equals("")){
            //Toast.makeText(context, "Por favor, introduzca una dirección IP y un PUERTO", Toast.LENGTH_LONG).show();
            edit1.setError("Introduzca una dirección IP");
            edit2.setError("Introduzca un PUERTO");
            return false;
        }else if(edit1.getText().toString().equals("")){
          //  Toast.makeText(context, "Por favor, introduzca una dirección IP", Toast.LENGTH_LONG).show();
            edit1.setError("Introduzca una dirección IP");
            return false;
        }else if(edit2.getText().toString().equals("")){
           // Toast.makeText(context, "Por favor, introduzca un PUERTO", Toast.LENGTH_LONG).show();
            edit2.setError("Introduzca un PUERTO");
            return false;
        }else{
           Toast.makeText(context, "Datos correctos", Toast.LENGTH_LONG).show();
            return true;
        }
    }
}
