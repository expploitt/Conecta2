package com.uvigo.expploitt.conecta2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Scanner;


public class Led extends AppCompatActivity {

    ImageView mLedImage;
    TextView mLedText;
    Button mLedOn;
    Button mLedOff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led);

        mLedImage = (ImageView)findViewById(R.id.led_imagen);
        mLedText = (TextView)findViewById(R.id.led_texto);
        mLedOn = (Button)findViewById(R.id.led_boton_on);
        mLedOff = (Button)findViewById(R.id.led_boton_off);

        GlobalParameters globalPar = (GlobalParameters)getApplicationContext();
        final String ip = globalPar.getIP();
        final int port = globalPar.getPORT();

        /**
         *
         */
        mLedOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] strings = {"LON", ip, String.valueOf(port)};
                new NetworkCommunication().execute(strings);
            }
        });
        /**
         *
         */
        mLedOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] strings = {"LOFF", ip, String.valueOf(port)};
                new NetworkCommunication().execute(strings);
            }
        });
    }

    public class NetworkCommunication extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {

            try {
                NetworkUtils.connecting(strings[0], strings[1], Integer.parseInt(strings[2]));
            } catch (IOException e) {
                e.printStackTrace();

            }
            return "";
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
        }
    }


}


