package com.uvigo.expploitt.conecta2;

import android.app.Application;

/**
 * Created by expploitt on 21/06/17.
 */

public class GlobalParameters extends Application {

    private  String IP;
    private  int PORT;

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getPORT() {
        return PORT;
    }

    public void setPORT(int PORT) {
        this.PORT = PORT;
    }
}
