package com.uvigo.expploitt.conecta2;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by expploitt on 21/06/17.
 */

public class NetworkUtils{


    public static void connecting(String message, String ip, int port) throws IOException {

        InetAddress ipAddr = InetAddress.getByName(ip);
        Socket socket = new Socket(ipAddr, port);
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.writeUTF(message);
        socket.close();
        return;

    }
}
