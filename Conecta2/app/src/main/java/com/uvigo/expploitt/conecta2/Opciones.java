package com.uvigo.expploitt.conecta2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Opciones extends AppCompatActivity {

    TextView mOpcionesTexto;
    Button mLedBoton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones);

        mOpcionesTexto = (TextView)findViewById(R.id.opciones_led_boton);
        mLedBoton = (Button)findViewById(R.id.opciones_led_boton);

        mLedBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Opciones.this, Led.class);
                startActivity(intent);
            }
        });
    }
}
